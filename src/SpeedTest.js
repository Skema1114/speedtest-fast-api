const FastSpeedtest = require("fast-speedtest-api");
const publicIp = require('public-ip');
require('dotenv').config()

module.exports = {
  async get(request, response) {
    let speedtestUpload = new FastSpeedtest({
      token: process.env.API_KEY,
      verbose: false,
      timeout: 10000,
      https: true,
      urlCount: 5,
      bufferSize: 8,
      testType: 'upload',
      unit: FastSpeedtest.UNITS.Mbps,
      proxy: 'http://optional:auth@my-proxy:123'
    });

    let speedtestDownload = new FastSpeedtest({
      token: process.env.API_KEY,
      verbose: false,
      timeout: 10000,
      https: true,
      urlCount: 5,
      bufferSize: 8,
      testType: 'download',
      unit: FastSpeedtest.UNITS.Mbps,
      proxy: 'http://optional:auth@my-proxy:123'
    });

    (async () => {
      await speedtestUpload.getSpeed().then(up => {
        upFinal = up;
        console.log('upload done...');
      }).catch(e => {
        console.error(e.message);
      });

      await speedtestDownload.getSpeed().then(down => {
        downFinal = down;
        console.log('download done...');
      }).catch(e => {
        console.error(e.message);
      });

      await publicIp.v4().then(v4 => {
        if (v4 != null) {
          v4Final = v4;
          console.log('ipv4 done...');
        }
      });

      await publicIp.v4().then(v6 => {
        if (v6 != null) {
          v6Final = v6;
          console.log('ipv6 done...');
        }
      });

      console.log(await v6Final == v4Final
        ? 'up ' + await upFinal + ' Mbps | down ' + await downFinal + ' Mbps | publicV4 ' + await v4Final + ' | publicV6 null'
        : 'up ' + await upFinal + ' Mbps | down ' + await downFinal + ' Mbps | publicV4 ' + await v4Final + ' | publicV6 ' + v6Final
      );
      response.json({
        'up': await upFinal, 'down': await downFinal,
        'unit': 'Mbps', 'publicV4': await v4Final,
        'publicV6': await v6Final == v4Final ? null : v6Final
      });
    })();
  }
}