const express = require('express');
const routes = express.Router();
const speedTest = require('./speedTest');

routes.get('/', speedTest.get);

module.exports = routes;